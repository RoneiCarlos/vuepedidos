import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
var nroPedido = 1
export default new Vuex.Store({
  state: {
    pedidos:{}
  },
  
  actions: {
    
    addPedido({commit}, pedido) {
      pedido.id = nroPedido
      pedido.alteracao = Date.now()
      pedido.status = "pedido"
      commit('addPedido', pedido)
      nroPedido += 1
      alert('Pedido - ' + pedido.id) 
    }
  },
  mutations: {
    addPedido(state, pedido) {
      state.pedidos[pedido.id] = pedido  
      
    }
  }
});